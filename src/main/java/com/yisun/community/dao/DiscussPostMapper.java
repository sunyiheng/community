package com.yisun.community.dao;

import com.yisun.community.entity.DiscussPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DiscussPostMapper {
    List<DiscussPost> selectDiscussPosts(int userId, int offset, int limit);

    // @Param注解用于给参数区别名
    // 如果只有一个参数并且在<if>里使用 必须加别名
    int selectDiscussPostRows(@Param("userId") int userId);
}
